<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table='posts';

    public function relUsers(){
        return $this->hasOne('App\Models\User','id','id_user');
    }

    protected $fillable=['id_user','title','url','teste_http'];
}
