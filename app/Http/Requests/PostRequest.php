<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'url'=>'required|url'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Coloque o título!',
            'url.url'  => 'A URL precisa estar no formato válido! Ex.: "http://www.minhaurl.com.br"',
        ];
    }

}
