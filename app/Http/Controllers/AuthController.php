<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\User;

class AuthController extends Controller{

    private $objUser;
    private $objPost;

    public function __construct(){
        $this->objUser=new User();
        $this->objPost=new Post();
    }

    public function dashboard(){
        if(Auth::check() === true) {
            //$post=Post::all()->sortBy('title');
            $post=Post::where(['id_user'=>Auth::user()->id])->get()->sortBy('id');
            return view('admin.dashboard',compact('post'));
        }
        return redirect()->route('admin.login');
    }

    public function ShowLoginForm(){
        return view('admin.login');
    }

    public function login(Request $request){

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::attempt($credentials)){
            return redirect()->route('admin');
        };

        return redirect()->back()->withInput()->withErrors(['Os dados informados são inválidos']);
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin');
    }

}
