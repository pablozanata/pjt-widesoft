<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

use Exception;

class GetHttpResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:httpresponse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtém o código de resposta e o corpo da resposta HTTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts=DB::table('posts')->get()->sortBy('id');

        foreach ($posts->all() as $post) {
            try{
                $response = Http::get($post->url);
                $StatusTeste = true;
            }
            catch(Exception $e){
                $StatusTeste = false;
            }

            if ($StatusTeste) {
                DB::table('posts')->where('id',$post->id)->update(['teste_http' => 'Codigo: '.$response->status().' | Resposta HTTP: "'.utf8_encode($response->body()).'"']);
            }else{
                DB::table('posts')->where('id',$post->id)->update(['teste_http' => 'Codigo: 404 Not Found']);
            }
        }
    }
}
