<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Rota para a página inicial
Route::get('/', HomeController::class)->name('site.home');

#Rotas para o CMS
Route::get('/admin',[AuthController::class,'dashboard'])->name('admin');
Route::get('/admin/login',[AuthController::class,'ShowLoginForm'])->name('admin.login');
Route::get('/admin/logout',[AuthController::class,'logout'])->name('admin.logout');
Route::post('/admin/login/do',[AuthController::class,'login'])->name('admin.login.do');

Route::resource('/posts', PostController::class);
