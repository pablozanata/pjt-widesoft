@extends('layouts.site')

@section('content')


<section class="cta__home">
    <div class="cta__wrapper">
        <h2>Login</h2>
        <form class="row g-3" method="post" action="{{route('admin.login.do')}}">
            @csrf

            @if($errors->all())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <div class="form-floating col-md-6">
                <input type="email" name="email" class="form-control" id="email" placeholder="E-mail" value="pablo.zanata@gmail.com" />
                <label for="email">E-mail </label>
            </div>
            <div class="form-floating col-md-6">
                <input type="password" name="password" class="form-control" id="password" placeholder="Senha" />
                <label for="password">Senha </label>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto">
                <button class="btn btn-light" type="submit" class="btn btn-light btn-lg">Login</button>
            </div>
        </form>
    </div>
        <div class="pattern"></div>
</section>
@endsection
