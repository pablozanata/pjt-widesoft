@extends('layouts.site')

@section('content')

<section class="cta__dashboard">

    <div class="cta__wrapper">
        <div class="alert alert-success" role="alert">
            <span class="fst-italic fs-6">Bem vindo(a)! <a href="{{route('admin.logout')}}">Logout</a></span>
        </div>
        <br/>
        <h2 class="title-large">Dashboard</h2>
        <p>Aqui você pode gerenciar as suas URLs rastreadas.</p>
    </div>
    <div class="pattern"></div>

</section>

<footer class="main_footer">
    <div class="dots_pattern"></div>
    <div class="main-wrapper flex-container">
        <div class="btn-group">
            <a class="btn btn-success" href="{{url("posts/create")}}">Cadastrar URL</a>
        </div>
    </div>
    <p></p>
    @if (session('success'))
        <div class="main-wrapper flex-container">
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        </div>
    @endif
    <div class="main-wrapper flex-container">
        @csrf
        <table class="table table-hover text-center">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">URL</th>
                    <th scope="col">Response</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>

            @foreach($post as $posts)
                @php
                    $user=$posts::where(['id'=>$posts->post_id])->get();
                @endphp
                <tbody>
                    <tr>
                        <th scope="row">{{ $posts->id }}</th>
                        <td>{{ $posts->title }}</td>
                        <td><a href="{{$posts->url}}" target="_blank">{{$posts->url}}</a></td>
                        <td>@if(isset($posts->teste_http))<i class="far fa-file-word"></i>@endif</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-primary" href="{{url("posts/$posts->id")}}">Ver</a>
                                <a class="btn btn-warning" href="{{url("posts/$posts->id/edit")}}">Editar</a>
                                <form method="POST" action="{{ url("posts/$posts->id")}}">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="delete-url btn btn-danger rounded-0 rounded-end" value="Excluir">
                                </form>
                            </div>
                        </td>
                    </tr>
                </tbody>
            @endforeach
        </table>
    </div>
</footer>

@endsection
