@extends('layouts.site')

@section('content')

<section class="cta__dashboard">

    <div class="cta__wrapper">
        <h2 class="title-large">@if(isset($post))Editar @else Cadastrar @endif URL</h2>
        <p></p>
    </div>

</section>

<footer class="main_footer">
    <div class="dots_pattern"></div>
        <div class="col-8 m-auto">
            @if(isset($errors) && count($errors)>0)
                <div class="text-center mt4 mb-4 p-2 alert-danger">
                    @foreach($errors->all() as $erro)
                        {{ $erro }}<br>
                    @endforeach
                </div>
            @endif
            @if(isset($post))
                <form name="formEdit" id="formEdit" method="post" action="{{url('posts/'.$post->id)}}">
                    @method('PUT')
            @else
                <form name="formCad" id="formCad" method="post" action="{{url('posts')}}">
            @endif
                @csrf
                <div class="form-floating">
                    <input class="form-control" type="text" name="title" id="title" placeholder="Título" required value="{{ $post->title ?? '' }}"><br>
                    <label for="title">Título</label>
                </div>
                <div class="form-floating">
                    <input class="form-control" type="text" name="url" id="url" placeholder="URL" required value="{{ $post->url ?? '' }}"><br>
                    <label for="url">URL</label>
                </div>
                <input class="form-control" type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                <input class="form-control" type="hidden" name="teste_http" id="teste_http" value="">
                <input class="btn btn-primary" type="submit" value="@if(isset($post))Editar @else Cadastrar @endif">
            </form>
        </div>
    </div>
    <div class="dots_pattern">
        <div class="col-8 m-auto">
            <a href="{{route('admin')}}" class="btn btn-link">Voltar</a>
        </div>
    </div>
</footer>

@endsection
