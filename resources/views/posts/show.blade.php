@extends('layouts.site')

@section('content')

@php
    $user=$post->find($post->id)->relUsers;
@endphp

<section class="cta__dashboard">

    <div class="cta__wrapper">
        <h2 class="title-large">Visualizar URL #{{ $post->id }}</h2>
        <p></p>
    </div>

</section>

<footer class="main_footer">
    <div class="dots_pattern"></div>
        <div class="col-8 m-auto">
            <p>
                <strong>Usuário:</strong> {{$user->name}}
            </p>

            <p>
                <strong>Post Id:</strong> {{ $post->id }}
            </p>
            <p>
                <strong>Título:</strong> {{$post->title}}
            </p>
            <p>
                <strong>URL:</strong> <a href="{{$post->url}}" target="_blank">{{$post->url}}</a>
            </p>
            <p>
                <strong>HTTP Response:</strong> {{$post->teste_http}}
            </p>
        </div>
    </div>
    <div class="dots_pattern">
        <div class="col-8 m-auto">
            <a href="{{route('admin')}}" class="btn btn-link">Voltar</a>
        </div>
    </div>
</footer>

@endsection
