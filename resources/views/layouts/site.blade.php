<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
    <head>
        <title>URL Tracking</title>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="theme-color" content="Blue"/>
        <meta charset="UTF-8"/>
        <meta name="description" content="Desafio técnico para vaga de Desenvolvedor Full Stack da WideSoft"/>
        <meta name="author" itemprop="PHCZ"/>

        <!-- Link Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <!-- Links -->
        <link rel="stylesheet" href="{{secure_asset('css/app.css')}}"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

        <link id="favicon" rel="shortcut icon" href="{{secure_asset('images/Favicon.svg')}}" sizes="16x16" type="image/svg">
        <link id="favicon" rel="shortcut icon" href="{{secure_asset('images/Favicon.svg')}}" sizes="32x32" type="image/svg">
        <link id="favicon" rel="shortcut icon" href="{{secure_asset('images/Favicon.svg')}}" sizes="48x48" type="image/svg">

    </head>

    <body>
        <nav>
            <div class="main-wrapper">
                <div class="flex-container">
                    <a class="logotipo" href="{{route('site.home')}}">
                        <img src="{{secure_asset('images/Logo.svg')}}">
                    </a>
                    <ul class="navigation__itens" id="menu">
                        <li>
                            <a href="{{route('site.home')}}">Home
                                <span class="border-effect"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin')}}">Dashboard
                                <span class="border-effect"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        <section class="sub__footer">
            <div class="main-wrapper flex-container">
                <a href="#" target="_blank">Design by <strong>Pablo Zanata</strong></a>
            </div>
        </section>


    </body>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script src="{{ url('assets/js/javascript.js') }}"></script>
</html>
