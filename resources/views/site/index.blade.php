@extends('layouts.site')

@section('content')

<header>
    <div class="hero">
        <div class="main-wrapper">
            <div class="hero__content">
                <h1 class="display-large">Nós rastreamos a sua URL.</h1>
                <a class="button button_sublte button_large" href="#info" role="button">Comece agora mesmo!</a>
            </div>
            <section class="docked-bar flex-container">
                <div class="hero__information" id="info">
                    <p>Não se preocupe, as informações estarão protegidas. Em caso de dúvidas nos ligue:</p>
                </div>
                <div class="talk-to-us">
                    <img src="{{asset('images/Phone-Icon.svg')}}" alt="Telefone Icone">
                    <a title="Clique no número do telefone para ligar" href="tel:(14) 99824-0907">(14) 99824-0907</a>
                </div>
            </section>
        </div>
    </div>
</header>

<section class="cta__home">
    <div class="cta__wrapper">
        <h2 class="title-large">Comece a rastrear agora</h2>
        <p>Com apenas alguns cliques você começa a monitorar as suas URLs, não se preocupe com valores, este serviço é 100% gratuíto.</p>
        <a href="{{route('admin.login')}}" role="button" class="button button_accent">Acessar painel</a>
    </div>
    <div class="pattern"></div>
</section>
@endsection
